document.addEventListener("DOMContentLoaded", function() {
    const btnShowModal = document.querySelector(".trainer__show-more");
    const modalTemplate = document.getElementById("modal-template");

    btnShowModal.addEventListener("click", function() {
        const modalClone = modalTemplate.content.cloneNode(true);
        document.body.appendChild(modalClone);

        const modalCloseButton = modalClone.querySelector(".modal__close");
        console.log(modalCloseButton)
        modalCloseButton.addEventListener("click", function() {
            modalClone.parentNode.removeChild(modalClone);
        });
    });
});
